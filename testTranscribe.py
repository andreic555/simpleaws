from __future__ import print_function
import time
import boto3
from botocore.errorfactory import ClientError
import os 
import json

import concurrent.futures
from concurrent.futures import ThreadPoolExecutor




#can use eviroment variables without calling getenv 
AWS_S3_CREDS = {
    "aws_access_key_id": os.getenv("AWS_ACCESS_KEY"), 
    "aws_secret_access_key": os.getenv("AWS_SECRET_KEY")
}


class simpleAWS():
    
    outputBucket = "afiless" 
    
    

    # credidentials configured with aws configure
    def __init__(self):
        # self.s3client  = boto3.client('s3', region_name='eu-central-1', **AWS_S3_CREDS)
        self.s3client  = boto3.client('s3')
        self.tclient =  boto3.client('transcribe') 
        self.comprehendClient = boto3.client('comprehend')
        self.s3r = boto3.resource('s3')
        
    
    def isComprehendFile(self, fileName):
        return fileName[0] == "K" or fileName[0] == "E" or fileName[0] == "S"
    
    def isTranscribedFile(self, fileName):
        return  "json" in  fileName and  any([char.isdigit() for char in fileName]) and not self.isComprehendFile(fileName)
        
    def getFileList(self, type = "audio"):
        bucket = self.s3r.Bucket(simpleAWS.outputBucket)
        
        if type == "audio":
            return (audioFile.key for audioFile in bucket.objects.all() if "mp3" in audioFile.key) 
        elif type == "transcribed":
            return (audioFile.key for audioFile in bucket.objects.all() if "json" in audioFile.key and self.isTranscribedFile(audioFile.key))
        elif type == "comprehend":
            return (audioFile.key for audioFile in bucket.objects.all() if "json" in audioFile.key and self.isComprehendFile(audioFile.key))
        else:
            raise ValueError("Unsuported type")
        
    def batchTranscribe(self):
        
        future_results = []
        with ThreadPoolExecutor(max_workers=24) as executor:   
            for audioFile in self.getFileList():
                print("Add %s to thread pool for transcribe" %audioFile)
                future_results.append(executor.submit(self.transcribe, (audioFile)))
            
            concurrent.futures.wait(future_results)
            
        
    def transcribe(self, fileName, outputfileName = None):
        
        
        job_uri = "s3://"+simpleAWS.outputBucket+"/"+fileName
        job_name = job_uri.rsplit('/', 1)[-1].split(".")[0]
        
        
        outputkey = job_name+".json" if outputfileName is None else outputfileName
        try:
            self.tclient.start_transcription_job(TranscriptionJobName = job_name, 
                           Media = {'MediaFileUri': job_uri}, 
                           MediaFormat = 'mp3', 
                           LanguageCode = 'en-US',
                           OutputBucketName = simpleAWS.outputBucket, 
                           OutputKey = outputkey
                           
                           )
        except Exception as e:
            print(e)


        while True:
            
            status = self.tclient.get_transcription_job(TranscriptionJobName = job_name)
            if status['TranscriptionJob']['TranscriptionJobStatus'] in  ['COMPLETED', 'FAILED']:
                break
            print("Not ready yet")
            time.sleep(5)
            

        print(status)
        print(status['TranscriptionJob']['Transcript']['TranscriptFileUri'])

        try:
            objContent = self.s3client.get_object(Bucket = simpleAWS.outputBucket, Key = outputkey)
        except Exception as e:
            print(e)
            objContent = self.s3client.get_object(Bucket = simpleAWS.outputBucket, Key = job_name+".json")
            
        
        
        # bytes
        body = objContent['Body'].read()
        
        # bytes to dict
        return json.loads(body)
    
    
    def getTextFromTranscribedFile(self, fileName):
        objContent = self.s3client.get_object(Bucket = simpleAWS.outputBucket, Key = fileName)
        dictFile = json.loads(objContent['Body'].read())
        text = dictFile['results']['transcripts'][0]['transcript']
        
        return text
    
    def comprehend(self, fileName, option = "E"):
        
        try:
            self.s3client.head_object(Bucket=simpleAWS.outputBucket, Key=option+fileName)
        #if comprehend file does not exists, generate it
        except ClientError:
    
            text = self.getTextFromTranscribedFile(fileName)
            
      
            if option == "K":
                transcriptJson = self.comprehendClient.detect_key_phrases(Text = text, LanguageCode = 'en')
            elif option == "E":
                transcriptJson = self.comprehendClient.detect_entities(Text = text, LanguageCode = 'en')
            elif option == "S":
                transcriptJson = self.comprehendClient.detect_syntax(Text = text, LanguageCode = 'en')
                
            self.s3client.put_object(Body = json.dumps(transcriptJson).encode('utf-8'), Bucket = simpleAWS.outputBucket, Key = option+fileName)
            
        
        objContent = self.s3client.get_object(Bucket = simpleAWS.outputBucket, Key = option+fileName)
        dictFile = json.loads(objContent['Body'].read())
        print(dictFile)
        return dictFile
        
    def batchComprehend(self, option = "E"):
        future_results = []
        with ThreadPoolExecutor(max_workers=24) as executor:   
            for tFile in self.getFileList(type = "transcribed"):
                print("Add %s to thread pool for comprehend" %tFile)
                future_results.append(executor.submit(self.comprehend, (tFile), (option)))
            concurrent.futures.wait(future_results)
        

 
        

s3 = boto3.resource('s3')

for bucket in s3.buckets.all():
    print(bucket.name)


obj = simpleAWS() 
# obj.batchTranscribe()
#

obj.transcribe("arabic20.mp3")
obj.comprehend("arabic20.json", "E")
#obj.batchTranscribe()
obj.batchComprehend("K")




